'use strict'

document.addEventListener('DOMContentLoaded', setUp);

// global variable(paragraph)
let para = document.querySelector('#quote');

/**
 * @author Yassine Ibhir
 * setUp function calls FetchApi when button is clicked
 */
function setUp(){
    let btn = document.querySelector('button');
    btn.addEventListener('click',fetchApi);
}

/**
 * @author Yassine Ibhir
 * call set transition to position img.
 * fetch Api takes the response object
 * call setQuote or set Erro depending on what the promise retrieved.
 */
function fetchApi(){
    
    setTransition();
    
    let url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";

    fetch(url).then(response => {
        if(!response.ok){
            throw new Error('status code:'+ response.status);
        }
       
        return response.json();
    })
    .then(json =>{
        setQuote(json);
        })
    .catch(error => setError(error));

}

/**
 * @author Yassine Ibhir
 *  put quote inside paragraph and set color to gold
 * @param {an array of one quote} obj 
 */
function setQuote(obj){
    para.style.color = "#b08f26" ;
    let quote = obj[0];
    para.textContent = quote;
}

/**
 * @author Yassine Ibhir
 * put error inside paragraph and ask user to try again
 * @param { represents the promise error} error 
 */
function setError(error){
    para.style.color = 'red';
    para.textContent = 'Please try again later :'+error;
}

/**
 * @author Yassine Ibhir
 * function changes id of image for transition
 */
function setTransition(){
    let img = document.querySelectorAll('img');
    if(img[1].id == 'RS'){
        img[1].id = 'RST';
    }

}


